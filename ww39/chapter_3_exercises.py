#Ex1
>>> hours = float(input("Enter Hours:"))
Enter Hours:45
>>> rate = float(input("Enter Rate:"))
Enter Rate:10
>>> extra=1.5
>>> if(hours<=40):
	pay=hours*rate

>>> if(hours>40):
	pay=40*rate+((hours-40)*rate*extra)

>>> print(pay)
475.0

#Ex2
try:
    hours = float(input("Enter Hours:"))
    rate = float(input("Enter Rate:"))
except:
    print("Error, please enter numeric input!")
    quit()

if(hours<=40):
    pay=hours*rate

if(hours>40):
	pay=40*rate+((hours-40)*rate*extra)

print(pay)

#Ex3
try:
    score=float(input("Enter score between 0.0 and 1.0:"))
    if(score>1.0 or score<0.0):
        print("Score is out of range")
    elif(score>=0.9):
        print("A")
    elif(score>=0.8):
        print("B")
    elif(score>=0.7):
        print("C")
    elif(score>=0.6):
        print("D")
    elif(score<0.6):
        print("F")
except:
    print("Score is out of range")
