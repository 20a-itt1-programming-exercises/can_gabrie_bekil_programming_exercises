# Ex1
#Write a while loop that starts at the last character in the string and works its way backwards to the first character in the string, printing each letter on a separate line, except backwards.
fruit="banana"
index=len(fruit)
while index>0
	letter=fruit[index]
	print(letter)
	index = index - 1

# Ex2
#Given that fruit is a string, what does fruit[:] mean?
#The whole string from start to end

#Ex3
#Encapsulate this code in a function named count,and generalize it so that it accepts the string and the letter as arguments.
word = 'banana'
def count(character, word):
	count = 0
	for letter in word:
		if(letter == character):
			count = count + 1
	return count
count=count('a', word)
print(count)

#Ex4
#There is a string method called count that is similar to the function in the previous exercise. Read the documentation of this method at:
#https://docs.python.org/library/stdtypes.html#string-methods
#Write an invocation that counts the number of times the letter a occurs in “banana”.**
word = 'banana'
count=word.count("a")
print(count)

#Ex5
#Take the following Python code that stores a string: str = 'X-DSPAM-Confidence:0.8475'
strpos=str.find(":")
substr=str[strpos+1:len(str)]
x=float(substr)
print(x)

#Ex6
#Read the documentation of the string methods at
# https://docs.python.org/library/stdtypes.html#string-methods You might want to experiment with some of them to make sure you understand how they work. strip and replace are particularly useful.
# The documentation uses a syntax that might be confusing. For example, in find(sub[, start[, end]]), the brackets indicate optional arguments.
# So sub is required, but start is optional, and if you include start, then end is optional.

