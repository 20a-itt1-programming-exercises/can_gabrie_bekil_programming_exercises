#Ex1
#Write a function called chop that takes a list and modifies it, removing the first and last elements, and returns None. Then write a function called middle that takes a list and returns a new list that contains all but the first and last elements.

def chop(list):
    del list[0]
    del list[-1]
    return None

def middle(list):
	chop(list)


#Ex2
#Figure out which line of the above program is still not properly guarded. See if you can construct a text file which causes the program to fail and then modify the program so that the line is properly guarded and test it to make sure it handles your new text file.

if len(words) < 3: continue


#Ex3
#Rewrite the guardian code in the above example without two if statements. Instead, use a compound logical expression usin the or logical operator with a single if statement.

if len(words) < 3 or words[0] != 'From': continue


#Ex4
#Download a copy of the file www.py4e.com/code3/romeo.txt. Write a program to open the file romeo.txt and read it line by line. For each line, split the line into a list of words using the split function. For each word, check to see if the word is already in a list. If the word is not in the list, add it to the list. When the program completes, sort and print the resulting words in alphabetical order.

list=[]
fhand = open('romeo.txt')
for line in fhand:
    words = line.split()                                
    for word in words:
        if word in list : continue                      
        list.append(word)                               
print(sorted(list))                                     


#Ex5
#Write a program to read through the mail box data and when you find line that starts with “From”, you will split the line into words using the split function. We are interested in who sent the message, which is the second word on the From line.

fhand = open('enter file name: ')                         
count = 0
for line in fhand:
    words = line.split()
    if len(words) < 3 or words[0] != 'From' : continue
    print(words[1])
    count += 1
print('There were %d lines in the file with From as the first word' % count)         


#Ex6
#Rewrite the program that prompts the user for a list of numbers and prints out the maximum and minimum of the numbers at the end when the user enters “done”. Write the program to store the numbers the user enters in a list and use the max() and min() functions to compute the maximum and minimum numbers after the loop completes.

list=[]
while True:
	try:
		inp=input("Input a number, write done if you are finished: ")
		if(inp=="done"): break
		number=float(inp)
		list.append(number)
	except:
		print("Incorrect input")
print('Max:', max(list), 'Min:',min(list))
