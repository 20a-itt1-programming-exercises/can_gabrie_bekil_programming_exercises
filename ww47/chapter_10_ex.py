#Ex1
#Revise a previous program as follows: Read and parse the “From” lines and pull out the addresses from the line. Count the number of messages from each person using a dictionary. After all the data has been read, print the person with the most commits by creating a list of (count, email) tuples from the dictionary. Then sort the list in reverse order and print out the person who has the most commits.

fhand = open('mbox-short.txt')
mails=dict()
for line in fhand:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	if(words[1] not in mails):
		mails[words[1]]=1
	else:
		mails[words[1]] = mails[words[1]] + 1
lst = list()
for key, val in list(mails.items()):
	lst.append((val, key))
lst.sort(reverse=True)
for key, val in lst:
	print(key, val)
print(mails)


#Ex 2
#This program counts the distribution of the hour of the day for each of the messages. You can pull the hour from the “From” line by finding the time string and then splitting that string into parts using the colon character. Once you have accumulated the counts for each hour, print out the counts, one per line, sorted by hour as shown below.

fhand = open('mbox-short.txt')
mails=dict()
for line in fhand:
	words = line.split()
	if(len(words)<2 or words[0]!='From') : continue
	time=words[5].split(":")
	time=time[0]
	if(time not in mails):
		mails[time]=1
	else:
		mails[time] = mails[time] + 1
lst = list()
for key, val in list(mails.items()):
	lst.append((key, val))
lst.sort(reverse=False)
for key, val in lst:
	print(key, val)


#Ex 3
#Write a program that reads a file and prints the letters in decreasing order of frequency. Your program should convert all the input to lower case and only count the letters a-z. Your program should not count spaces, digits, punctuation, or anything other than the letters a-z. Find text samples from several different languages and see how letter frequency varies between languages. Compare your results with the tables at https://wikipedia.org/wiki/Letter_frequencies.

import re

fhand = open('mbox-short.txt')
letters=dict()
for line in fhand:
	x = re.findall('[a-zA-Z]', line)
	if(len(x)>0):
		for letter in x:
			letter=letter.lower()
			if(letter not in letters):
				letters[letter]=1
			else:
				letters[letter] = letters[letter] + 1
lst = list()
for key, val in list(letters.items()):
	lst.append((val, key))
lst.sort(reverse=True)
for key, val in lst:
	print(val,": ",key)
