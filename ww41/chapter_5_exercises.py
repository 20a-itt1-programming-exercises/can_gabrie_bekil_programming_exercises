#Ex1
Write a program which repeatedly reads numbers until the
user enters “done”. Once “done” is entered, print out the total, count,
and average of the numbers. If the user enters anything other than a
number, detect their mistake using try and except and print an error
message and skip to the next number.

total = 0
count = 0
average = 0                                                              

while True:
  try:
    x = input("Enter a number: ")                                        
    if (x == "done"):                                                    
     break                                                               
    value = float(x)                                                     
    total = value + total                                                    
    count = count + 1
    average = total / count
  except:
    print("Invalid input.")
print (total, count, average)

#Ex2
Write another program that prompts for a list of numbers
as above and at the end prints out both the maximum and minimum of
the numbers instead of the average.

total = 0
count = 0
max = None                                       
min = None

while True:
  try:
    x = input("Enter a number: ")
    if (x == "done"): 
     break
    value = float(x)
    total = value + total
    count = count + 1
    if max is None or value>max:
        max=value
    if min is None or value<min:
        min=value
  except:
    print("Invalid input.")
print (total, count, max, min)

