def add(num1,num2):
    sum = float(num1) + float(num2)
    return sum

def subtract(num1,num2):
    sum = float(num1) - float(num2)
    return sum

def divide(num1,num2):
    try:
        sum = float(num1) / float(num2)
    except ZeroDivisionError:
        print("cannot divide by zero, please try again")
        main()
    return sum

def multiply(num1,num2):
    sum = float(num1) * float(num2)
    return sum


def main():
    try:
        while True:
            userNum1Inp = input("Enter the first number:" + "\n" + "> ")
            if str(userNum1Inp) == "done":
                print("Goodbye!")
                break
            elif userNum1Inp == "":
                continue
            userNum2Inp = input("Enter the second number:" + "\n" + "> ")
            if str(userNum2Inp) == "done":
                print("Goodbye!")
                break
            elif userNum2Inp == "":
                continue
            try:
                userChoiceInp = input("would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?"  + "\n" + "> ")
                if str(userChoiceInp) == "done":
                    print("Goodbye!")
                    break
                elif int(userChoiceInp) == 1:
                    print(f"{userNum1Inp} added with {userNum2Inp} = {add(userNum1Inp,userNum2Inp)}")
                elif int(userChoiceInp) == 2:
                    print(f"{userNum1Inp} subtracted from {userNum2Inp} = {subtract(userNum1Inp,userNum2Inp)}")
                elif int(userChoiceInp) == 3:
                    print(f"{userNum1Inp} divided with {userNum2Inp} = {divide(userNum1Inp,userNum2Inp)}")
                elif int(userChoiceInp) == 4:
                    print(f"{userNum1Inp} multiplied with {userNum2Inp} = {multiply(userNum1Inp,userNum2Inp)}")
                else:
                    print("Number is not between 1 and 4")
            except ValueError:
                print("only numbers and 'done' is accepted as input, please try again")
                continue
    except:
        print("Goodbye!")
main()
